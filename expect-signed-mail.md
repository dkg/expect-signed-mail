---
title: Expect Signed Mail
docname: draft-dkg-lamps-expect-signed-mail-02
category: info

ipr: trust200902
area: int
workgroup: lamps
keyword: Internet-Draft
submissionType: IETF

venue:
  group: "LAMPS"
  type: "Working Group"
  mail: "spasm@ietf.org"
  arch: "https://mailarchive.ietf.org/arch/browse/spasm/"
  repo: "https://gitlab.com/dkg/expect-signed-mail"
  latest: "https://dkg.gitlab.io/expect-signed-mail/"

stand_alone: yes
pi: [toc, sortrefs, symrefs]

author:
 -
    ins: D. K. Gillmor
    name: Daniel Kahn Gillmor
    org: American Civil Liberties Union
    email: dkg@fifthhorseman.net
 -
    ins: A. Wussler
    name: Aron Wussler
    org: Proton AG
    country: Switzerland
    email: aron@wussler.it

--- abstract

This draft proposes a mechanism for e-mail users to indicate to their peers that the peer should expect all future e-mail from them to be cryptographically signed.

--- middle

# Introduction

E-mail signature validation is hard.
When an e-mail signature is absent (or invalid), a reasonable mail user agent will hide their cryptographic authenticity security indicator for the message.
But an absent security indicator is hard to notice.

Some e-mail users create end-to-end signatures of all of their e-mails.
The peers of those users may want to display a more significant warning message when a signature is absent or invalid.

This draft proposes a mechanism whereby an e-mail author can indicate to a peer that they should expect all their future messages to be cryptographically signed.

## Requirements Language

{::boilerplate bcp14-tagged}

## Terminology

The terms Message Submission Agent (MSA), Message Transfer Agent (MTA), and Message User Agent (MUA) are to be interpreted as defined in {{!RFC6409}}.

# Signalling Mechanism

A sender that intends to signal their intention to activate the Expected-Signed mechanism MUST add an "Expected-Signed" header to the outgoing email, specifying an expiration date in the value of the header.

The authenticity of the header MUST be validated.
The header SHOULD be transmitted in the protected headers (see {{!I-D.ietf-lamps-header-protection-16}}), and a recipient MUA SHOULD deem it valid if the signing key is already trusted.
Alternatively, the recipient's MUA:

- SHOULD validate the DKIM headers {{!RFC6376}} and SHOULD require them to be in the protected headers.

- SHOULD validate the SPF records {{!RFC7208}} to verify it is coming from an authorized source and SHOULD require the message to be delivered over TLS before deeming it valid.

FIXME: Do we want to support them all?

A recipient MUA that receives a valid "Expected-Signed" header SHOULD safely store this information and its expiration date indexed per email address.

## Expect-Signed syntax

The ABNF (Augmented Backus-Naur Form) syntax for the Expect-Signed header field is given below, as described by {{!RFC2616}}.

    Expect-Signed = "Expect-Signed" ":"
                    [ directive ]  *( ";" [ directive ] )

    directive                 = directive-name [ "=" directive-value ]
    directive-name            = token
    directive-value           = token | quoted-string

Note that:

- The Expect-Signed header field name and directive names are not case sensitive.

- All directives MUST appear only once in an Expect-Signed header field.
  Directives are either optional or required, as stipulated in their definitions.

- The order of appearance of directives is not significant.

- MUAs MUST ignore any Expect-Signed header field containing directives, or other header field value data, that does not conform to the syntax defined in this specification.

- If an Expect-Signed header field contains directive(s) not recognized by the MUA, the MUA MUST ignore the unrecognised directives.
If the Expect-Signed header field otherwise satisfies the above requirements, the MUA MUST process the recognized directives.

### The expiry directive

The expiry directive defines an expiration date for the expectation of a signature.
The policy MUST be enforced until the expiry date is in the past.

This value is expressed by the sender with a fixed-length and single-zone subset of the date and time specification used by the Internet Message Format {{!RFC5322}}.

    expiry-name  = "expiry"
    expiry-value = IMF-fixdate

This value SHOULD be safely stored by the recipient’s MUA, and SHOULD be updated when a valid newer one is received.

NOTE: any date in the past will effectively cease the policy enforcement.

## Header Examples

The Expect-Signed header stipulates an Expect-Signed policy to remain in effect until the specified date:

    Expect-Signed: expiry="Sun, 20 Oct 2019 14:19:20 GMT";

NOTE: the expiry-value must be quoted since it is not a token.

FIXME: should the expiry use a more sane date format, like ISO-8601?

# Validating Policy

All e-mails coming from addresses that are stored and valid as Expect-Signed MUST be validated.
To validate a message's signature the recipient's client MUST follow OpenPGP's specification {{Section 5.2.4 of !RFC4880}}.

FIXME: This is not only OpenPGP.
Should include a reference to PGP/MIME {{!RFC3156}}, S/MIME {{!RFC8551}}, and {{!I-D.ietf-lamps-e2e-mail-guidance-12}}.

The sender's key can be retrieved from any trusted storage or repository, and if none is found it SHOULD be indicated in the feedback.
This mechanism will allow the sender to automatically reply with their key.

# On Policy Violation

In the scenario where a sender has enabled the Expect-Signature it is expected that all the outgoing messages are provided with a a valid signature, and both the sender and recipient should be notified when a signature is missing or invalid.

An MSA, MTA, or MUA SHOULD NOT prevent a message from being received due to a missing signature.
The MUA MUST warn the user if an expected signature is missing or invalid, and SHOULD provide feedback as specified in the following sections.

## Warn

The recipient's MUA MUST warn the user that the signature is missing or invalid on every instance where the signature is expected but not verified.
The two cases SHOULD be treated as equal, because a missing signature is not any more suspicious than a broken signature: a malicious attacker that alters a message can easily remove the signature too.

## Explicit Feedback

FIXME: describe TLSRPT-style feedback

The MUA MAY avoid automatic explicit feedback, as it introduces a vector for attackers to know if an email is reachable or if a user read the message.

### Sender behaviour

FIXME: Define what to do with the explicit feedback.
FIXME: Key points: Should it be machine or human readable? Localization?

## Inline Feedback

When replying to a message whose expectation of signature is failed the MUA SHOULD introduce an `Expect-Signed-Failure` header to signal to the original sender that the message signature was missing or invalid.

The syntax of the `Expect-Signed-Failure` field, described by the ABNF {{!RFC2616}} is as follows:

    "Expect-Signed-Failure" ":" failure-reason CRLF

Note that the Expect-Signed-Failure header field name and failure-reason value are not case sensitive.

### Failure-reason

There are four categories of failure for signature verification:

- **no-signature**: The message is not provided with a signature packet or part.

- **signature-invalid**: The message is provided with a not matching signature, and the key ID matches the signature key ID, i.e., the content is different from the signed data.

- **signature-not-verified**: The message is provided with a signature, but the MUA is unable to verify it because it does not have or can not retrieve the matching key.

- **signature-expired**: The message has a corresponding signature, that is invalid because either the key or signature expiration are in the past.
In this case an MUA SHOULD NOT give any feedback to the sender.

The failure-reason value can then assume the following values:

    failure-reason =  (   "no-signature"
                        / "signature-invalid"
                        / "signature-not-verified"
                        / "signature-expired"
                      )

### Sender behaviour

The sender's MUA receiving an inline feedback MUST display a warning to the user if the reason is "no-signature" or "signature-invalid", and MAY display a warning if the reason is "signature-not-verified" or "signature-expired".

The purpose of this warning is to warn the sender that there might be some misconfigured option in the mail client, that result in messages being unsigned or malformed, or that he is victim of an impersonation.

Furthermore, when receiving an inline feedback with reason "signature-not-verified" the sender's MUA MAY automatically attach a copy of their public key to a successive reply.

# Common UX for Absent and Invalid Signatures

FIXME: explain why receiving MUAs should display the same thing when a
signature is missing as when it is absent.

# Related Work

This draft is inspired by (and similar to) HSTS, MTA-STS, TLSRPT, etc.

FIXME: include references

# IANA Considerations

IANA might need to register the e-mail headers `Expect-Signed` and `Expect-Signed-Failure`.

--- back

# Acknowledgements

FIXME

# Mapping the Solution Space

\[ RFC Editor: please remove this section before publication ]

The range of possible solutions in this problem space is potentially quite wide.

The draft attempts to make some decisions, but they can be revisted.
This appendix tries to document some distinct axes along which the problem can be resolved.

The completed draft should provide a clear choice along each axis, or a mechanism for some active participant in the protocol to select a choice.

## Signal Location

Where should the signal be emitted?
Is it a per-message signal?
Is it in the sender's certificate?
Is it in the DNS?

## Signal Scope

What is the scope of the signal?
For example, does it cover a particular e-mail address in the "From" field?
Could it cover all e-mail addresses in a given domain?
Or does it only cover a specific pair-wise promise (e.g., "alice@example.com will sign all mail that is only addressed to bob@example.net")?
Does it apply to all mail, or could it be limited to end-to-end-encrypted mail?

## Intervening Mail User Agents

How does this signal interact with messages that arrive through intervening MUAs, like mailing lists or bug-tracking systems that may (deliberately or not) break signatures while forwarding mail?

## How to Signal?

How does the sender opt into emitting this signal such that all of their MUAs are aware of it?
Clearly, you'd want each MUA controlled by the sender to know that the signal has been published so that they can all adjust their signing policy.

## Retraction

How does the sender change their mind once such a signal has been emitted?
Does the signal expire?
What happens to messages during the period where the signal is in some sort of indeterminate state?

## Consequences

What should the available consequences be when an unsigned (or broken-signature) message arrives from a sender who has emitted that signal?
Should the recieving MUA show the message with a warning?
Should the receiving MUA report the failure to the sender (e.g., like MTA-STS)?
Should they reject the message entirely?
How much control should the signaller be able to exercise?

## What Kind of Cryptographic Signature?

Does the signal commit the sender to any particular kind of cryptographic signature?
For example, PGP/MIME, or S/MIME?
To signatures verifiable by any particular certificate?

# Document Considerations

\[ RFC Editor: please remove this section before publication ]

## Document History

### Changes from draft-dkg-lamps-expect-signed-mail-00 to draft-dkg-lamps-expect-signed-mail-01

- add Aron Wussler to authors
